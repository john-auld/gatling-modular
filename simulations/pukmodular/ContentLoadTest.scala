package pukModular

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import Config._
import BrowseContent._


class ContentLoadTest extends Simulation {

  val nbUsers = Integer.getInteger("users", 1)
  val myRamp  = java.lang.Long.getLong("ramp", 1L)


  val anonContent = scenario("anon").exec(BrowseContent.randomPages)


  setUp(
    	   anonContent.inject(
    		     constantUsersPerSec(nbUsers.toDouble) during(myRamp minutes)
    	   )
      ).protocols(Config.httpConf)

}
