package pukModular

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import Config._


object Search {

	val headers_0 = Map(
		"Cache-Control" -> "max-age=0",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map(
		"Cache-Control" -> "max-age=0",
		"Origin" -> Config.BaseUrl,
		"Upgrade-Insecure-Requests" -> "1")

 val Site = exec(http("Site-root")
			.get("/")
			.headers(headers_0)
			.check(status.is(200))
		)
		.pause(12)

		.exec(http("post-search")
			.post("/")
			.headers(headers_1)
			.formParam("search_block_form", "levadopa")
			.formParam("op", "Search")
			.formParam("form_build_id", "form-s_cSz_iXFYcB9RT6eD1ylwhuZFb9A1zIbtQN2ZFK3wE")
			.formParam("form_id", "search_block_form")
		)

}
