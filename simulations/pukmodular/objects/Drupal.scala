package pukModular

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import Secrets._

object Drupal {

    // Login to Drupal
    // form_build_id is not a CSRF token - any value accepted
    val login = exec(http("login-page")
        .get("/user/login")
        )
      .pause(2,10)
      .exec(http("login-post")
        .post("/user/login")
        .formParam("name", Secrets.DrupalUser)
        .formParam("pass", Secrets.DrupalPassword)
        .formParam("form_build_id", "form-5OWLYr-8r0sfwu8V2-vX_iaM4LskzCdU1B_VjgsU1kA")
        .formParam("form_id", "user_login")
        .formParam("op", "Log in"))
      .pause(2,10)


    // Logout of Drupal
    val logout = exec(http("user-logout")
      .get("/user/logout")
      )

  }
