package pukModular

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

object BrowseContent {

  // Data source
  val accessLog = tsv("puk-200-content.tsv").random.convert {
    case ("status", string) => string.toInt
  }

  val randomPages = feed(accessLog)
      .exec(http("Content-page")
        .get("${url}")
        .check(status.is("${status}"))
      )

}
