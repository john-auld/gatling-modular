package pukModular

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import Config._

object Forum {

  val headers_0 = Map(
    "Origin" -> Config.BaseUrl,
    "Upgrade-Insecure-Requests" -> "1")

  // Simulation
  val comment = exec(http("forum-page")
        .get("/forum")
      ).pause(2)
      .exec(http("forum-symptoms")
        .get("/forum/symptoms")
      ).pause(2,10)
      .exec(http("forum-thread")
        .get("/forum/thread/80389")
        .check(css("""#comment-form > div > input[type=\"hidden\"]:nth-child(3)""", "value").saveAs("form_build_id"))
        .check(css("""#comment-form > div > input[type=\"hidden\"]:nth-child(4)""", "value").saveAs("form_token"))
      ).pause(2,10)

      .exec(http("forum-post")
        .post("/comment/reply/80389")
        .headers(headers_0)
        .formParam("comment_body[und][0][value]", "Gatling load test - Sift Digital")
        .formParam("form_build_id", "${form_build_id}")
        .formParam("form_token", "${form_token}")
        .formParam("form_id", "comment_node_forum_form")
        .formParam("op", "Post"))
      .pause(2,10)

}
