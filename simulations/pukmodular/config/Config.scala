package pukModular

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import java.time._
import java.time.format._


object Config {

  val  BaseUrl = "http://puk-preprod.siftdigital.net"

  val currentTime = LocalDateTime.now()
  val formatter   = DateTimeFormatter.ofPattern("HHmm")
  val timeStamp   = currentTime.format(formatter)

  val  UserAgent = "Sift-Digital-Gatling-2.2.2-".concat(timeStamp)

  var Proxy2 = "http://aws2"
  var Proxy3 = "http://aws3"
  var Proxy4 = "http://aws4"

  // http configuration
  val httpConf = http
    //.baseURLs(Config.Proxy2, Config.Proxy3, Config.Proxy4)
    .baseURL(Config.BaseUrl)
    .userAgentHeader(Config.UserAgent)
    //.disableFollowRedirect
    //.inferHtmlResources
    //.silentResources
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .extraInfoExtractor(extraInfo => List(
      "M: ", extraInfo.response.header("X-Micro-Cache" ),
      "D: ", extraInfo.response.header("X-Drupal-Cache"),
      extraInfo.request)
      )

}
