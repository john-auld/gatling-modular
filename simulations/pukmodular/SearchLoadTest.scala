package pukModular

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

import Config._
import Search._

class SearchLoadTest extends Simulation {

	val SearchSite  = scenario("Search-Site").exec(Search.Site)

	setUp(
		SearchSite.inject( atOnceUsers(1) )
    //SingleDonation.inject(rampUsers(1) over (1 seconds))
    //BackgroundTraffic.inject(rampUsersPerSec(1) to(10) during(5 minutes))
  ).protocols(Config.httpConf)

}
