package pukModular

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import Config._
import Drupal._
import BrowseContent._

class ContentMixedLoadTest extends Simulation {

  val nbUsers = Integer.getInteger("users", 1)
  val myRamp  = java.lang.Long.getLong("ramp", 1L)

  val mixedUsers = scenario("Mixed-Users").randomSwitch(
      95.0 -> exec(BrowseContent.randomPages),
      3.0  -> exec(Drupal.login, BrowseContent.randomPages, Drupal.logout),
      2.0  -> exec(Drupal.login, Forum.comment, Drupal.logout)
    )

  setUp(
    	   mixedUsers.inject(
    		     constantUsersPerSec(nbUsers.toDouble) during(myRamp minutes)
    	   )
      ).protocols(Config.httpConf)

}
