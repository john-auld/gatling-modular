package pukModular

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import Config._
import Drupal._
import Forum._

class ForumLoadTest extends Simulation {

  val nbUsers = Integer.getInteger("users", 1)
  val myRamp  = java.lang.Long.getLong("ramp", 0L)

  val authUsers = scenario("authenticated").exec(Drupal.login, Forum.comment, Drupal.logout)


	setUp(
			authUsers.inject(
					atOnceUsers(1)
					//rampUsers(nbUsers) over(myRamp minutes)
			)
		).protocols(Config.httpConf)

}
