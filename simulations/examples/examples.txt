
// not sure where this import is needed
//import scala.concurrent.duration._


// Java Cast's - http://www.tutorialspoint.com/java/lang/java_lang_long.htm
val nbUsers = Integer.getInteger("users", 1)
val myRamp  = java.lang.Long.getLong("ramp", 1L)


// Connect to multiple backends
val httpConf = http
  .baseURLs(Config.Proxy2, Config.Proxy3, Config.Proxy4)


// Debug messages to console
println(nbUsers.toString)
println(myRamp.toString)


// read OS environment variable
val startTime = System.getProperty("startTime")


// Percentages must add up to 100
val mixedUsers = scenario("Mixed-Users").randomSwitch(
    95.0 -> exec(BrowseContent.randomPages),
    3.0  -> exec(Drupal.login).exec(BrowseContent.randomPages).exec(Drupal.logout),
    2.0  -> exec(Drupal.login).exec(Forum.comment).exec(Drupal.logout)
  )


// Repeat - no random range provided by Gatling
val mixedUsers = scenario("Mixed-Users").randomSwitch(
    95.0 -> exec(BrowseContent.randomPages),
    3.0  -> exec(Drupal.login).repeat(3) { exec(BrowseContent.randomPages) }.exec(Drupal.logout),
    2.0  -> exec(Drupal.login).exec(Forum.comment).exec(Drupal.logout)
)


// throttle to a constant load
setUp(
      scn.inject(
              //rampUsers(nbUsers) over(myRamp seconds)
              constantUsersPerSec(5) during(11 minutes))).throttle(
    reachRps(5) in (60 seconds),
    holdFor(10 minute)
    //jumpToRps(10),
    //holdFor(2 minutes)
).protocols(httpConf)
